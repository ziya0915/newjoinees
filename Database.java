
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
/**
 * This is the implementation of the class database that
 * perform the operations create table
 * @author Ziya1
 */
public class Database {
	String jdbcUrl = "jdbc:mysql://localhost/ziya";
	String username = "root";
	String password = "Ziya@0915";

	/**
	 * This is helper method to Implement the creation of the table
	 */

	public boolean CreateTable(String tablename) {
		String Query = "CREATE TABLE joinees(" + "UserName VARCHAR(225)," + "ID INT ," + "OTP INT,"
				+ "Recovery_code VARCHAR(255)," + "Firstname VARCHAR(225)," + "Lastname VARCHAR(255),"
				+ "Department VARCHAR(255)," + "Location VARCHAR(255)," + "PRIMARY KEY (ID))";
		try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password);
				Statement stmt = conn.createStatement();) {
			        stmt.executeUpdate(Query);
			System.out.println("Table " +tablename+" created sucessfully in the database...." );
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}
	/**
	 * This is the helper method to read data from the file
	 */
	
	@SuppressWarnings("resource")
	public boolean readDataFromTheFile() {
		File file=new File("C:\\Users\\tashaik\\eclipse-workspace\\Newjoinees\\src\\newJoinees.txt");
		try (Connection conn = DriverManager.getConnection(jdbcUrl, username, password);
				Statement stmt = conn.createStatement();) {
		try {
			
		      BufferedReader br = new BufferedReader(new FileReader(file));
		      String st;

             while ((st = br.readLine()) != null) {
               System.out.println(st);
               String[] split = st.split(";");
               String username="",firstname="",lastname="",Department="",Location="",
            		   id="",otp="",recovery_code="";
               
		
               username       = split[0];
               id             = split[1];
               otp            = split[2];
               recovery_code  = split[3];
               firstname      = split[4];
               lastname       = split[5];
               Department     = split[6];
               Location       = split[7];
               
               stmt.executeUpdate("insert into newJoinees values('" + username + "','" + id + "','" + otp + "','"
						+ recovery_code + "'," + "'" + firstname + "','" + lastname + "','" + Department + "','"
						+ Location + "')");
			}
			System.out.println("data added to table successfully");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}

	return true;
}

	}


