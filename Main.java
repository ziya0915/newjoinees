import java.util.Scanner;

/**
 * This is the implementation of the class main which 
 * perform all the used operations
 * @author ziya
 */
public class Main {
		     public static void main(String[] args) {
		    	Database db = new Database();
		
		    	 Scanner sc = new Scanner(System.in);
		    	 System.out.println("1. CREATE TABLE");
		    	 System.out.println("2. READ TEXT FILE");
		    	 System.out.println("enter choice for operation");
			     int ch=sc.nextInt();
		   switch(ch) {
		   
		   case 1:  // create a table by calling method of createTable
				      db.CreateTable("newJoinees");
				      break;
		   case 2: //read and update data to database
			          db.readDataFromTheFile();
			          break;
		   }
		   sc.close();
		}
	}
